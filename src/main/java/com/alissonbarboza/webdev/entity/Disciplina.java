package com.alissonbarboza.webdev.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Disciplina implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	private String codigo;
	private String nome;
	private int qtdCreditos;
	
	@ManyToMany(targetEntity = Professor.class, fetch = FetchType.LAZY, mappedBy = "disciplinas")
	private List<Professor> professores = new ArrayList<>();
	
	public Disciplina() {}
	
	public Disciplina(String codigo, String nome, List<Professor> professores, int qtdCreditos) {
		super();		
		this.codigo = codigo;
		this.nome = nome;
		this.professores = professores;
		this.qtdCreditos = qtdCreditos;
	}
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@JsonIgnore
	public List<Professor> getProfessores() {
		return professores;
	}

	public void setProfessores(List<Professor> professores) {
		this.professores = professores;
	}

	public int getQtdCreditos() {
		return qtdCreditos;
	}

	public void setQtdCreditos(int qtdCreditos) {
		this.qtdCreditos = qtdCreditos;
	}
	
}
