package com.alissonbarboza.webdev.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
public class Professor implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private String matricula;

	private String email;
	private String nome;

	@ManyToMany(cascade = CascadeType.ALL, targetEntity = Disciplina.class)
	@JoinTable(name = "professor_disciplina", joinColumns = @JoinColumn(name = "disciplina.id"), 
		inverseJoinColumns = @JoinColumn(name = "professor.matricula"))
	private List<Disciplina> disciplinas = new ArrayList<Disciplina>();

	public Professor() {
	}

	public Professor(String matricula, String email, String nome, List<Disciplina> disciplinas) {
		super();
		this.matricula = matricula;
		this.email = email;
		this.nome = nome;
		this.disciplinas = disciplinas;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Disciplina> getDisciplinas() {
		return disciplinas;
	}

	public void setDisciplinas(List<Disciplina> disciplinas) {
		this.disciplinas = disciplinas;
	}

}
