package com.alissonbarboza.webdev;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.alissonbarboza.webdev.entity.Disciplina;
import com.alissonbarboza.webdev.entity.Professor;
import com.alissonbarboza.webdev.repository.DisciplinaRepository;
import com.alissonbarboza.webdev.repository.ProfessorRepository;

@SpringBootApplication
public class WebDevApplication implements CommandLineRunner{

	@Autowired
	private DisciplinaRepository disciplinaRepository;
	
	@Autowired
	private ProfessorRepository professorRepository;
	
	public static void main(String[] args){
		SpringApplication.run(WebDevApplication.class, args);
	}
	
	@Override
	public void run(String... args) throws Exception {
		
		Professor p1, p2, p3;
		p1 = new Professor("2016107492", "manoel@gmail.com.br", "Manoel Afonso", null);
		p2 = new Professor("2016107493", "llingo@gmail.com.br", "Lucas Lingo", null);
		p3 = new Professor("2016107494", "quarto@gmail.com.br", "Jonathas Quarto", null);
		
		professorRepository.saveAll(Arrays.asList(p1, p2, p3));
		
		Disciplina d1, d2, d3;
		d1 = new Disciplina("INF-104", "Computação", null, 60);
		d2 = new Disciplina("INF-105", "Engenharia", null, 60);
		d3 = new Disciplina("INF-106", "Matematica", null, 60);
		
		disciplinaRepository.saveAll(Arrays.asList(d1, d2, d3));	
		
		p1.setDisciplinas(Arrays.asList(d1,d2));
		p2.setDisciplinas(Arrays.asList(d2,d3));
		p3.setDisciplinas(Arrays.asList(d1,d2,d3));
		
		professorRepository.saveAll(Arrays.asList(p1,p2,p3));
		
		d1.setProfessores(Arrays.asList(p1,p3));
		d2.setProfessores(Arrays.asList(p1,p2,p3));
		d3.setProfessores(Arrays.asList(p2,p3));
		
		disciplinaRepository.saveAll(Arrays.asList(d1,d2,d3));
	}

}
