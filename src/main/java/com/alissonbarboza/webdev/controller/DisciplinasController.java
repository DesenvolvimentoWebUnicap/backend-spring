package com.alissonbarboza.webdev.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alissonbarboza.webdev.entity.Professor;
import com.alissonbarboza.webdev.service.DisciplinaService;

@RestController
@RequestMapping(path="/disciplinas")
public class DisciplinasController {
	
	@Autowired
	private DisciplinaService disciplinaService;
	
	@RequestMapping(method= RequestMethod.GET)
	public ResponseEntity<?> findAll(){
		return ResponseEntity.ok().body(disciplinaService.findAll());
	}
	
	@GetMapping("/teste/{id}")
	public List<Professor> lala(@PathVariable ("id") int id){
		return disciplinaService.findProfessoresByDisciplinas(id);
	}
	
}
