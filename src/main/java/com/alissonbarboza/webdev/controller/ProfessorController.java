package com.alissonbarboza.webdev.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alissonbarboza.webdev.entity.Professor;
import com.alissonbarboza.webdev.service.DisciplinaService;
import com.alissonbarboza.webdev.service.ProfessorService;

@RestController
@RequestMapping(path="/professores")
@CrossOrigin
public class ProfessorController {
	
	@Autowired
	ProfessorService professorService;
	@Autowired
	private DisciplinaService disciplinaService;
	
	@GetMapping
	public ResponseEntity<?> findAll(){
		return ResponseEntity.ok().body(professorService.findAll());
	}
	
	@GetMapping("/{matricula}")
	public ResponseEntity<?> findByMatricula(@PathVariable String matricula){
		return ResponseEntity.ok().body(disciplinaService.findByMatricula(matricula));
	}
	
	@PostMapping
	public ResponseEntity<?> save(@RequestBody Professor professores){
		return ResponseEntity.ok().body(professorService.save(professores));
	}
	
}
