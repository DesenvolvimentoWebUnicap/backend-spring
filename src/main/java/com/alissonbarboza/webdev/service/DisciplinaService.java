package com.alissonbarboza.webdev.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alissonbarboza.webdev.entity.Disciplina;
import com.alissonbarboza.webdev.entity.Professor;
import com.alissonbarboza.webdev.repository.DisciplinaRepository;
import com.alissonbarboza.webdev.repository.ProfessorRepository;

@Service
public class DisciplinaService {
	
	@Autowired
	DisciplinaRepository disciplinaRepository;
	
	@Autowired
	ProfessorRepository professorRepository;
	
	public List<Disciplina> findAll(){
		List<Disciplina> obj = disciplinaRepository.findAll();
		return obj;
	}
	
	public List<Disciplina> findByMatricula(String matricula){		
		Professor prof = professorRepository.findByMatricula(matricula);
		return prof.getDisciplinas();
		
	}
	
	public List<Professor> findProfessoresByDisciplinas(int id){
		return disciplinaRepository.getProfessorByDisciplina(id);
	}
	
}
