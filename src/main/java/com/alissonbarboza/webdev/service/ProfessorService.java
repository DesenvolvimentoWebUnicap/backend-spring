package com.alissonbarboza.webdev.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alissonbarboza.webdev.entity.Professor;
import com.alissonbarboza.webdev.repository.ProfessorRepository;


@Service
public class ProfessorService {

	@Autowired
	private ProfessorRepository professorRepository;
		
	public List<Professor> findAll(){
		return professorRepository.findAll();
	}
	public  Professor save(Professor professor){
		return professorRepository.save(professor);
	}
}
