package com.alissonbarboza.webdev.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.alissonbarboza.webdev.entity.Disciplina;
import com.alissonbarboza.webdev.entity.Professor;

@Repository
public interface DisciplinaRepository extends JpaRepository<Disciplina, Integer> {
	
	@Query(value= "select professor.matricula, professor.email, professor.nome from PROFESSOR_DISCIPLINA inner join PROFESSOR on PROFESSOR_DISCIPLINA.disciplina_id = PROFESSOR.matricula where :disciplinaId = PROFESSOR_DISCIPLINA.disciplina_id",nativeQuery=true)
	List<Professor> getProfessorByDisciplina(@Param ("disciplinaId") int id);
}
