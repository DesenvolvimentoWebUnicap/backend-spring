package com.alissonbarboza.webdev.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.alissonbarboza.webdev.entity.Professor;

public interface ProfessorRepository extends JpaRepository<Professor, String> {
	public Professor findByMatricula(String matricula);
}
